import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.ArrayList;
public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		Scanner leer = new Scanner(System.in);
		ArrayList<String> libros = new ArrayList<String>();
		Libro libro1;
		Libro libro2;
		Libro libro3;
		Libro libro4;
		String nombre;
		String autor;
		int valor;
		int largo;
		libro1 = new Libro("Drácula", "Bram Stroker");
		libro2 = new Libro("Don Quijote de la mancha", "Miguel de Cervantes");
		libro3 = new Libro("Moby Dick", "Herman Melville");
		libros.add("Drácula");
		libros.add("Don Quijote de la mancha");
		libros.add("Moby Dick");
		System.out.println("Ingrese el nombre del libro: ");
		nombre = reader.readLine();
		System.out.println("Ingrese el nombre del autor del libro: ");
		autor = reader.readLine();
		libro4 = new Libro(nombre, autor);
		libros.add(nombre);
		largo = libros.size();
		System.out.println("Libros registrados: ");
		for (int i=0; i<largo; i++)
			System.out.println(libros.get(i));
		System.out.println("\n");
		System.out.println("Ejemplo de información: ");
		System.out.println(libro1.getNombre()+" - "+libro1.getAutor());
		System.out.println("Cantidad: "+libro1.getCantidad());
		System.out.println("Prestados: "+libro1.getPrestados());
		System.out.println("\nIngrese nueva cantidad: ");
		valor = leer.nextInt();
		libro1.setCantidad(valor);
		System.out.println("Ingrese cuantos libros ha prestado: ");
		valor = leer.nextInt();
		libro1.setPrestados(valor);
		
		System.out.println("\n"+libro1.getNombre()+" - "+libro1.getAutor());
		System.out.println("Cantidad: "+libro1.getCantidad());
		System.out.println("Prestados: "+libro1.getPrestados());
		
		
		
		
	}

}
