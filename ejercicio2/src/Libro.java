
public class Libro {
	private String nombre;
	private String autor;
	private int cantidad;
	private int prestados;

	
	
	Libro(String param_nombre, String param_autor){
		this.nombre = param_nombre;
		this.autor = param_autor;
		this.cantidad = 1;
		this.prestados = 0;
	}
	String getNombre() {
		return this.nombre;
	}
	String getAutor() {
		return this.autor;
	}
	int getCantidad() {
		return this.cantidad;
	}
	int getPrestados() {
		return this.prestados;
	}
	void setCantidad(int nueva_cantidad) {
		this.cantidad = nueva_cantidad;
	}
	void setPrestados(int nuevo_numero)	{
		this.prestados = nuevo_numero;
	}
}
