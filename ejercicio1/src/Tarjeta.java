
public class Tarjeta {
	private String nombre;
	private String apellido;
	private int saldo;
	
	Tarjeta(String param_nombre, String param_apellido){
		this.nombre = param_nombre;
		this.apellido = param_apellido;
		this.saldo = 0;
		
	}
	
	String getNombre() {
		return this.nombre;	
	}
	String getApellido() {
		return this.apellido;
	}
	int getSaldo() {
		return this.saldo;
	}
	void setSaldo(int nuevo_saldo) {
		this.saldo = nuevo_saldo;
		return;
	}
}
