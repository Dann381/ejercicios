import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.ArrayList;
public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		ArrayList<String> tarjetas = new ArrayList<String>();
		Scanner reader = new Scanner(System.in);
		Tarjeta tarjeta1;
		Tarjeta tarjeta2;
		tarjeta1 = new Tarjeta("Daniel", "Tobar");
		tarjeta2 = new Tarjeta("Daniel", "Tobar");
		tarjetas.add("tarjeta1");
		tarjetas.add("tarjeta2");
		System.out.println(tarjetas);
		System.out.println("Titular: "+tarjeta1.getNombre()+" "+ tarjeta1.getApellido());
		System.out.println("Saldo: "+tarjeta1.getSaldo());
		int nuevo_saldo = 0;
		System.out.println("Ingrese nuevo saldo para tarjeta 1: ");
		nuevo_saldo = reader.nextInt();
		tarjeta1.setSaldo(nuevo_saldo);
		System.out.println("Nuevo saldo: "+tarjeta1.getSaldo());
	}

}
